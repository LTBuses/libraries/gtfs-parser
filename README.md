# LTBuses GTFS Parse

This is a simple parser the takes a stream containing a GTFS-formatted zip
file, and extracts the data relevant to the LTBuses applications.

It uses [unzip-stream](https://www.npmjs.com/package/unzip-stream) to extract
the data on the fly, without having to completely unpack the entire zip file.

The current result format is *very specific* to what is needed for the LTBuses
apps, which currently only supports the LTC buses in London, Ontario.

## Installation

```sh
npm install @ltbuses/gtfs-parser
```

## Usage

```js
const fs = require('fs');
const GtfsParser = require('@ltbuses/gtfs-parser');

const transitZip = fs.createReadStream('google_transit.zip');
const gtfsParser = new GtfsParser(transitZip);

gtfsParser.getData().then((gtfs) => {
  console.log(gtfs.getStops());
});
```

See the [developer documentation](https://ltbuses.gitlab.io/docs/gtfs-parser/)
for full details.
