/**
 * A more friendly wrapper around parsed GTFS data.
 *
 * The raw data contains information on many different schedules. This wrapper
 * stores a date and filters the provided information to only those routes,
 * stops, and schedules that apply.
 */
class GtfsData {

  /**
   * Creats a GTFS-format date, with a matching weekday name.
   *
   * @private
   *
   * @param {any} [...options=The current date]
   *   Arguments to pass on to `new Date()`.
   *
   * @return {GtfsData~date}
   */
  static getDate(...options) {
    const days = [
      'sunday',
      'monday',
      'tuesday',
      'wednesday',
      'thursday',
      'friday',
      'saturday',
    ];
    const date = new Date(...options);
    const year = date.getFullYear();
    const weekday = days[date.getDay()];
    let month = (date.getMonth() + 1).toString(10);
    let day = date.getDate().toString(10);

    if (month.length < 2) {
      month = `0${month}`;
    }

    if (day.length < 2) {
      day = `0${day}`;
    }

    /**
     * @typedef GtfsData~date
     * @private
     * @property {string} date - A date in the format "YYYYMMDD"
     * @property {string} weekday - The full name of the weekday for the date.
     */
    return {
      date: `${year}${month}${day}`,
      weekday: weekday,
    };
  }

  /**
   * Create the wrapper from a raw data object and a date..
   *
   * @param {Object} rawData
   *   The object storing bus data that is created with {@link GtfsParser}.
   * @param {any} [...options=The current date]
   *   Arguments to pass on to `new Date()`.
   */
  constructor(rawData, ...dateOptions) {
    this.setDate(...dateOptions);
    this.rawData = rawData;
    this.cache = new Map();
  }

  /**
   * Get the raw data. This is mostly useful for caching.
   *
   * @return {Object}
   *  The raw data originally passed to the constuctor.
   */
  getRawData() {
    return this.rawData;
  }

  /**
   * Change the date to filter by.
   *
   * @param {any} [...options=The current date]
   *   Arguments to pass on to `new Date()`.
   */
  setDate(...dateOptions) {
    const date = GtfsData.getDate(...dateOptions);
    this.weekday = date.weekday;
    this.date = date.date;
    this.clearCache();
  }

  /**
   * Clear the filter caches.
   *
   * The GtfsData object stores the results of it's date-based filtering in
   * a map, so that it doesn't have to re-do it on every request.
   *
   * @private
   */
  clearCache() {
    this.cache = new Map();
  }

  /**
   * Returns a list of all stops with active schedules.
   *
   * @return {GtfsData~stop[]}
   */
  getStops() {
    if (this.cache.has('stops')) {
      return this.cache.get('stops');
    }

    const schedules = this.getActiveSchedules();
    const routeNames = this.rawData.routes;

    const stops = Array.from(this.rawData.stops)
      .map(([number, info]) => ({
        number: number,
        id: info.id,
        name: info.name,
        lat: info.location[0],
        lon: info.location[1],
        routes: new Map(info.schedules.reduce((allRoutes, [id, routes]) => {
          if (schedules.includes(id)) {
            return allRoutes.concat(routes.map(([routeNumber]) => ([
              routeNumber,
              routeNames.get(routeNumber),
            ])));
          }
          return allRoutes;
        }, [])),
      }));

    this.cache.set('stops', stops);

    /**
     * @typedef GtfsData~stop
     * @property {number} number - The stop number.
     * @property {string} name - The stop name.
     * @property {Map} routes - Bus route dynamic names keyed by route number.
     */
    return stops;
  }

  /**
   * Gets a list of all schedules for the active date.
   *
   * @private
   *
   * @return {string[]}
   *   The schedule ids.
   */
  getActiveSchedules() {
    if (this.cache.has('schedules')) {
      return this.cache.get('schedules');
    }

    const {date, weekday} = this;
    const [active, skipped] = this.rawData.holidays.get(date) || [[], []];

    const schedules = Array.from(this.rawData.schedules.entries())
      .reduce((list, entry) => {
        const [name, valid] = entry;
        const inRange = valid.start_date <= date && valid.end_date >= date;
        const rightDay = valid[weekday] === '1';

        if (inRange && rightDay && !list.includes(name)) {
          list.push(name);
        }
        return list;
      }, active)
      .filter(name => !skipped.includes(name));

    this.cache.set('schedules', schedules);
    return schedules;
  }

  /**
   * Get a list of all active routes.
   *
   * @return {GtfsData~route}
   */
  getRoutes() {
    return [...this.rawData.routes.entries()].map(
      /**
       * @typedef GtfsData~route
       * @property {number} number - The route number.
       * @property {string} name - The generic route name.
       */
      ([number, name]) => ({number, name})
    );
  }
}

module.exports = GtfsData;
