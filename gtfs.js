#!/usr/bin/env node

const GtfsParser = require('./GtfsParser');
const fs = require('fs');

const transitFileName = process.argv[2] || './google_transit.zip';

// Force the parser into "Full" mode (include complete schedules).
process.env.GTFS_FULL_SCHEDULE = true;

(async () => {
  const parser = new GtfsParser(fs.createReadStream(transitFileName));
  const gtfsData = await parser.getData();
  const data = gtfsData.getRawData();
  const quickStops = Array.from(data.stops).map(([id, stopData]) => ([
    id,
    {
      id: stopData.id,
      name: stopData.name,
      location: stopData.location,
      schedules: stopData.schedules.map(([schedule, routes]) => ([
        schedule,
        routes.map(([routeNumber]) => [
          routeNumber,
          data.routes.get(routeNumber),
        ]),
      ])),
    },
  ]));
  fs.writeFileSync('./bus_data.json', JSON.stringify({
    holidays: Array.from(data.holidays),
    schedules: Array.from(data.schedules),
    stops: quickStops,
    routes: Array.from(data.routes),
  }));
  fs.writeFileSync('./schedules.json', JSON.stringify(Array.from(data.stops)));
  console.log('Files generated.');
})();
