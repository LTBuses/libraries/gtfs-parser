const csv = require('csv-parser');
const getReducers = require('./reducers');
const GtfsData = require('./GtfsData');
const stream = require('stream');
const unzip = require('unzip-stream');

/**
 * Create a transform stream to pass CSVs to the right reducers.
 *
 * Each file entry is checked against the reducers map, and if there's a key
 * that matched the file name, `csv-parser` is used to stream the rows into
 * the appropriate reduce stream. The resulting array is passed along.
 *
 * @method GtfsParser.getArchiveParser
 * @private
 *
 * @param {Map} reducers
 *   A map of reduce streams keyed by filename.
 *
 * @return {external:stream~Transform}
 *   The stream.
 *
 * @see {@link https://github.com/mafintosh/csv-parser}
 */
const getArchiveParser = reducers => stream.Transform({
  objectMode: true,
  transform: function transform(entry, encoding, callback) {
    if (entry.type === 'File' && reducers.has(entry.path)) {
      entry
        .pipe(csv())
        .pipe(reducers.get(entry.path))
        .on('data', (result) => {
          this.push(result);
          callback();
        });
    }
    else {
      entry.autodrain();
      callback();
    }
  },
});

/**
 * Transform stream: Formats the bus data.
 *
 * The cross-references in the CSVs can't be followed as they're being parsed,
 * because of the streaming. Once all the collections are together, though, they
 * can be. This moves the data about when each bus stops at each stop into the
 * stops themselves for faster lookup, and then removes the global references.
 * It also sorts the routes by number.
 *
 * @type {external:stream~Transform}
 * @class
 * @private
 */
const cleanParsedData = stream.Transform({
  objectMode: true,

  /**
   * Transforms the bus data.
   *
   * @method
   * @name cleanParsedData#_transform
   *
   * @param {Object} busData
   *   The raw, extracted arrays from the CSVs.
   *
   * @return {Object}
   *   The cleaned-up and re-sorted bus data.
   */
  transform: function transform(busData, encoding, callback) {
    busData.stops.forEach((stop) => {
      const id = stop.id;
      stop.schedules = Array.from((busData.stopTrips.get(id) || [])
          .map(({trip, time}) => ({trip: busData.trips.get(trip), time: time}))
          .reduce((map, {trip, time}) => {
            const schedule = new Map(map.get(trip.schedule) || []);
            const routeNumber = busData.routes.get(trip.route)[0];
            if (process.env.GTFS_FULL_SCHEDULE) {
              const stopSchedule = schedule.get(routeNumber) || [];
              stopSchedule.push({label: trip.label, time: time});
              schedule.set(routeNumber, stopSchedule);
            }
            else {
              schedule.set(routeNumber, trip.label);
            }
            map.set(trip.schedule, Array.from(schedule));
            return map;
          }, new Map()));
    });
    delete busData.trips;
    delete busData.stopTrips;

    const routeSort = ([a], [b]) => a - b;
    busData.routes = new Map([...busData.routes.values()].sort(routeSort));

    this.push(busData);
    callback();
  },
});

/**
 * Uses streams to Parse GTFS data.
 *
 * @example <caption>Loading a local GTFS file</caption>
 *   const fs = require('fs');
 *   const parser = new GtfsParser(fs.createReadStream('google_transit.zip'));
 *   parser.getData().then((data) => {
 *      // Do something with the data.
 *   });
 *
 * @example <caption>Loading a remote GTFS file</caption>
 *   const request = require('request');
 *   const parser = new GtfsParser(request('http://example.com/google_transit.zip'));
 *   parser.getData().then((data) => {
 *      // Do something with the data.
 *   });
 *
 * @class
 */
class GtfsParser {

  /**
   * Create the parser from a GTFS file.
   *
   * @see {@link https://developers.google.com/transit/gtfs/reference/}
   *
   * @param {external:stream~Readable} fileStream
   *   A readable stream of a GTFS zip file.
   */
  constructor(fileStream) {
    this.loadData(fileStream);
  }

  /**
   * Updates the internal data from a new stream.
   *
   * @param {external:stream~Readable}
   */
  loadData(fileStream) {
    const reducers = getReducers();
    this.promise = new Promise((resolve, reject) => {
      fileStream.pipe(unzip.Parse())
        .pipe(getArchiveParser(reducers))
        .pipe(reducers.get('combine'))
        .pipe(cleanParsedData)
        .on('error', (error) => { reject(error); })
        .on('data', (data) => {
          resolve(data);
        });
    });
  }

  /**
   * Retrieves the parser's data, when it's ready.
   *
   * The retrieved data will be set for the current date, unless a specific date
   * is provided.
   *
   * @example <caption>Getting the GTFS data for june 8th, 2017</caption>
   *   parser.getData(2017, 5, 8).then((data) => {
   *    // Do something with the data.
   *   });
   *
   * @param {any} [...dateOptions=The current date]
   *   Arguments to pass on to `new Date()`.
   *
   * @return {Promise.<GtfsData>}
   *   A Promise that resolves with the GTFS data object once the input stream
   *   is parsed.
   */
  getData(...dateOptions) {
    return this.promise.then(data => new GtfsData(data, ...dateOptions));
  }
}

module.exports = GtfsParser;
