/**
 * A collection of reduce streams to transform specific GTFS files.
 *
 * The maps generated by these streams are taged with a name, and the special
 * [combine reducer]{@link reducers~combine} can use it to assemble them all
 * into a sigle object.
 *
 * @namespace reducers
 * @private
 * @see {@link https://developers.google.com/transit/gtfs/reference/}
 */

const streamReduce = require('stream-reduce');

/**
 * Helper to quickly create a Map with a name property.
 *
 * The [combine reducer]{@link reducers~combine} uses the name property to make
 * sure all the maps end up under the right keys in the final data object.
 *
 * @memberof reducers
 * @private
 * @see {@link reducers~combine}
 *
 * @param {string} name
 *   The name to assign the map.
 *
 * @return {Map}
 *   A new Map with a name.
 */
const getNamedMap = (name) => {
  const map = new Map();
  Object.defineProperty(map, 'name', {value: name});
  return map;
};

/**
 * Returns a map of reduce streams, keyed by the file they're meant to process.
 *
 * @method reducers~getReducers
 *
 * @return {Map}
 *   Reduce streams keyed by filename.
 */
module.exports = () => {
  const reducers = new Map();

  /**
   * @see {@link https://developers.google.com/transit/gtfs/reference/calendar-file}.
   * @name reducers~"calendar.txt"
   */
  reducers.set('calendar.txt', streamReduce((schedules, data) => {
    const id = data.service_id;
    delete data.service_id;
    schedules.set(id, data);
    return schedules;
  }, getNamedMap('schedules')));

  /**
   * @name reducers~"calendar_dates.txt"
   * @see {@link https://developers.google.com/transit/gtfs/reference/calendar_dates-file}.
   */
  reducers.set('calendar_dates.txt', streamReduce((holidays, data) => {
    if (typeof data.date !== 'undefined') {
      const holiday = holidays.get(data.date) || [[], []];
      holiday[data.exception_type - 1].push(data.service_id);
      holidays.set(data.date, holiday);
    }
    return holidays;
  }, getNamedMap('holidays')));

  /**
   * @name reducers~"routes.txt"
   * @see {@link https://developers.google.com/transit/gtfs/reference/routes-file}
   */
  reducers.set('routes.txt', streamReduce((routes, data) => {
    const routeId = parseInt(data.route_id, 10);
    const routeNumber = parseInt(data.route_short_name, 10);
    routes.set(routeId, [routeNumber, data.route_long_name]);
    return routes;
  }, getNamedMap('routes')));

  /**
   * @name reducers~"stops.txt"
   * @see {@link https://developers.google.com/transit/gtfs/reference/stops-file}
   */
  reducers.set('stops.txt', streamReduce((stops, data) => {
    stops.set(data.stop_code, {
      id: data.stop_id,
      name: data.stop_name,
      location: [data.stop_lat, data.stop_lon],
    });
    return stops;
  }, getNamedMap('stops')));

  /**
   * @name reducers~"stop_times.txt"
   * @see {@link https://developers.google.com/transit/gtfs/reference/stop_times-file}
   */
  reducers.set('stop_times.txt', streamReduce((stopTrips, data) => {
    const tripList = stopTrips.get(data.stop_id) || [];
    tripList.push({trip: data.trip_id, time: data.departure_time});
    stopTrips.set(data.stop_id, tripList);
    return stopTrips;
  }, getNamedMap('stopTrips')));

  /**
   * @name reducers~"trips.txt"
   * @see {@link https://developers.google.com/transit/gtfs/reference/trips-file}
   */
  reducers.set('trips.txt', streamReduce((trips, data) => {
    trips.set(data.trip_id, {
      schedule: data.service_id,
      route: parseInt(data.route_id, 10),
      label: data.trip_headsign,
    });
    return trips;
  }, getNamedMap('trips')));

  /**
   * Assemble the maps from the other streams into a single object.
   * @name reducers~combine
   */
  reducers.set('combine', streamReduce((ob, map) => {
    ob[map.name] = map;
    return ob;
  }, {}));

  return reducers;
};
